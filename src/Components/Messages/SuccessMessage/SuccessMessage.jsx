import React from 'react';
import './SuccessMessage.sass';

const SuccessMessage = (props) => {
    return (
        <div className="success">
            {props.text}
        </div>
    );
}

export default SuccessMessage;