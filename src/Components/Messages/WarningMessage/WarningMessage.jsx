import React from 'react';
import './WarningMessage.sass';

const WarningMessage = (props) => {

    return (
        <div className="warning">
            {props.text}
        </div>
    );
};

export default WarningMessage;