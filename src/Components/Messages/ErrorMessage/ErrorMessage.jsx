import React from 'react';
import './ErrorMessage.sass';

const ErrorMessage = (props) => {
    return (
        <div className="error">
            {props.text}
        </div>
    );
}

export default ErrorMessage;