import React from 'react';
import '../../../Forms/Forms.sass';
import { useHistory } from 'react-router-dom';

const ButtonForm = (props) => {

    const history = useHistory();

    const { isValueName, isValuePass, isRegistration, isFocusUsername, onFocusName, isFocusPass, onFocusPass, isValueNameRegistration, isValuePassRegistration,  onAuthorized, onWarningMessage, onErrorMessage } = props.store;

    function handleClick(event) {
        event.preventDefault();

        if (isValueName.length > 0 && isValuePass.length > 0 && isRegistration === true && isValueName === isValueNameRegistration && isValuePass === isValuePassRegistration) {
            history.push("/");

            onFocusName();
            onFocusPass();
            onAuthorized();
        }

        if(isValueName !== isValueNameRegistration && isFocusPass !== isValuePassRegistration) {
            onErrorMessage();

            setTimeout(() => {
                onErrorMessage();
            }, 2000);    
        }

    }

    function handleMessage(event) {
        event.preventDefault();

        if (isRegistration === false) {
            
            onWarningMessage();

            setTimeout(() => {
                onWarningMessage();
            }, 2000);
        }

    }

    return (
        <div className="area-btn">
            <input type="submit"
                className="area-btn__submit"
                style={isFocusUsername === true && isFocusPass === true ? { border: '2px solid #dfaf3b', color: '#dfaf3b' } : null}
                onClick={isRegistration === true ? handleClick : handleMessage}
            />
        </div>
    );
};

export default ButtonForm;

