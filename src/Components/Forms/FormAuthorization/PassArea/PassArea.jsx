import React from 'react';
import '../../../Forms/Forms.sass';
// import './PassArea.sass';

const PassArea = (props) => {

    const { isFocusPass, onFocusPass, onValuePass, } = props.store;

    const classesFocusPass = ['area__label'];


    if(isFocusPass) {
        classesFocusPass.push('area-focus');
    }

    return (
        <div className="area">
            <label htmlFor="password" className={classesFocusPass.join(' ')} style={isFocusPass === true ? {color: '#dfaf3b'} : {color: '#b6b0b0'}} >
                Password:
            </label>

            <input type="password"
                id="password"
                style={isFocusPass === true ? {borderBottom: '2px solid #dfaf3b'} : null}
                className="area__input"
                autoComplete="on"
                required
                onFocus={event => event.target.value ? null : onFocusPass()}
                onBlur={event => event.target.value ? null : onFocusPass()}
                onChange={event => event.target.value.length >= 0 ? onValuePass(event.target.value) : null }
                
            />
        </div>
    );
};

export default PassArea;