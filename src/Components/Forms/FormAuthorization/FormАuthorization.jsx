import React from 'react';
import '../../Forms/Forms.sass';
import { connect } from 'react-redux';
import { addFocusName, addFocusPass, addValueName, addValuePass, addAuthorized } from '../../../redux/actions/actionsFormAuthorization';
import { addWarningMessage } from '../../../redux/actions/actionWarningMessage';
import NameArea from './NameArea/NameArea';
import PassArea from './PassArea/PassArea';
import ButtonForm from './ButtonForm/ButtonForm';
import { addRegistration, addValueNameRegistration, addValuePassRegistration, } from '../../../redux/actions/actionsFormRegistration';
import { addErrorMessage } from '../../../redux/actions/actionErrorMessage';
import { Link } from 'react-router-dom';
import { addSuccessMessage } from '../../../redux/actions/actionSuccessMessage';
import SuccessMessage from '../../Messages/SuccessMessage/SuccessMessage';
import WarningMessage from '../../Messages/WarningMessage/WarningMessage';
import ErrorMessage from '../../Messages/ErrorMessage/ErrorMessage';

const FormАuthorization = (props) => {

    function handleClick() {
        
        if(props.isFocusUsername === true && props.isFocusPass === true) {
            props.onFocusName();
            props.onFocusPass();
        }

        if(props.isRegistration === true && props.isSuccessMessage === false) {
            props.onSuccessMessage();

            setTimeout(() => {
                props.onSuccessMessage();
            }, 2000);
        }

    }

    return (
        <React.Fragment>
            {!props.isAuthorized
                ? <form className="form">
                    <NameArea store={props} />
                    <PassArea store={props} />

                    {props.isRegistration === false
                        ? <Link to="/registration" className="registration-link" onClick={handleClick}>Registration</Link>
                        : <span className="registration-link" onClick={handleClick}>Registration</span>
                    }

                    <ButtonForm store={props} />

                    {props.isWarningMessage === true
                    ? <div className="area form__area form__area_warning">
                        <WarningMessage text="You should registration!" />
                    </div>
                    : null
                    }

                    {props.isRegistration === true && props.isErrorMessage === true
                    ? <div className="area form__area form__area_error">
                        <ErrorMessage text="Name Or Password Is Wrong!" />
                     </div>
                    : null
                    }

                    {props.isSuccessMessage === true 
                    ? <div className="area form__area form__area_success">
                        <SuccessMessage text="You registrated!" />
                      </div>
                    : null
                    }

                </form>

                : null
            }
        </React.Fragment>
    );
};


function mapStateToProps(state) {

    return {
        isFocusUsername: state.reducerFormAuthorization.isFocusUsername,
        isFocusPass: state.reducerFormAuthorization.isFocusPass,
        isValueName: state.reducerFormAuthorization.isValueName,
        isValuePass: state.reducerFormAuthorization.isValuePass,
        isAuthorized: state.reducerFormAuthorization.isAuthorized,
        isRegistration: state.reducerFormRegistration.isRegistration,
        isValueNameRegistration: state.reducerFormRegistration.isValueNameRegistration,
        isValuePassRegistration: state.reducerFormRegistration.isValuePassRegistration,
        isWarningMessage: state.reducerWarningMessage.isWarningMessage,
        isErrorMessage: state.reducerErrorMessage.isErrorMessage,
        isSuccessMessage: state.reducerSuccessMessage.isSuccessMessage,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onFocusName: () => dispatch(addFocusName()),
        onFocusPass: () => dispatch(addFocusPass()),
        onValueName: value => dispatch(addValueName(value)),
        onValuePass: value => dispatch(addValuePass(value)),
        onAuthorized: () => dispatch(addAuthorized()),
        onRegistration: () => dispatch(addRegistration()),
        onValueNameRegistration: value => dispatch(addValueNameRegistration(value)),
        onValuePassRegistration: value => dispatch(addValuePassRegistration(value)),
        onWarningMessage: () => dispatch(addWarningMessage()),
        onErrorMessage: () => dispatch(addErrorMessage()),
        onSuccessMessage: () => dispatch(addSuccessMessage()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormАuthorization);