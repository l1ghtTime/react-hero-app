import React from 'react';
import '../../../Forms/Forms.sass';

const NameArea = (props) => {

    const { isFocusUsername, onFocusName, onValueName } = props.store;

    const classesFocusName = ['area__label'];

    if (isFocusUsername) {
        classesFocusName.push('area-focus');
    }

    return (
        <div className="area form__area">
            <label htmlFor="username" className={classesFocusName.join(' ')} style={isFocusUsername === true ? { color: '#dfaf3b' } : { color: '#b6b0b0' }}>
                Name:
                </label>

            <input type="text"
                style={isFocusUsername === true ? { borderBottom: '2px solid #dfaf3b' } : null}
                id="username"
                className="area__input"
                required
                autoComplete="off"
                onFocus={event => event.target.value ? null : onFocusName()}
                onBlur={event => event.target.value ? null : onFocusName()}
                onChange={event => event.target.value.length >= 0 ? onValueName(event.target.value) : null}
            />
        </div>
    );
};

export default NameArea;

