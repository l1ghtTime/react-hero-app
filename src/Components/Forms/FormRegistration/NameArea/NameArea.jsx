import React from 'react';
import '../../../Forms/Forms.sass';

const NameArea = (props) => {

    const { isFocusUsernameRegistration, onFocusUsernameRegistration, onValueNameRegistration } = props.store;

    const classesFocusName = ['area__label'];

    if (isFocusUsernameRegistration) {
        classesFocusName.push('area-focus');
    }

    return (
        <div className="area">
            <label htmlFor="username" className={classesFocusName.join(' ')} style={isFocusUsernameRegistration  ? { color: '#dfaf3b' } : { color: '#b6b0b0' }}>
                Name:
                </label>

            <input type="text"
                style={isFocusUsernameRegistration ? { borderBottom: '2px solid #dfaf3b' } : null}
                id="username"
                className="area__input"
                autoComplete="off"
                required
                onFocus={event => event.target.value ? null : onFocusUsernameRegistration()}
                onBlur={event => event.target.value ? null : onFocusUsernameRegistration()}
                onChange={event => event.target.value.length >= 0 ? onValueNameRegistration(event.target.value) : null}
            />
        </div>
    );

}

export default NameArea;