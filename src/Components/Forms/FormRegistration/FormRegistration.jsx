import React from 'react';
import { connect } from 'react-redux';
import { addFocusNameRegistration, addFocusPassRegistration, addValueNameRegistration, addValuePassRegistration, addRegistration } from '../../../redux/actions/actionsFormRegistration';
import NameArea from '../FormRegistration/NameArea/NameArea';
import PassArea from '../FormRegistration/PassArea/PassArea';
import ButtonForm from '../FormRegistration/ButtonForm/ButtonForm';
import { addAuthorized } from '../../../redux/actions/actionsFormAuthorization';


const FormRegistration = (props) => {

    return (
        <form className="form">
            <NameArea store={props} />
            <PassArea store={props} />
            <ButtonForm store={props} />
        </form>
    );
};

function mapStateToProps(state) {

    return {
        isFocusUsernameRegistration: state.reducerFormRegistration.isFocusUsernameRegistration,
        isFocusPassRegistration: state.reducerFormRegistration.isFocusPassRegistration,
        isValueNameRegistration: state.reducerFormRegistration.isValueNameRegistration,
        isValuePassRegistration: state.reducerFormRegistration.isValuePassRegistration,
        isRegistration: state.reducerFormRegistration.isRegistration,
        isAuthorized: state.reducerFormAuthorization.isAuthorized,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onFocusUsernameRegistration: () => dispatch(addFocusNameRegistration()),
        onFocusPassRegistration: () => dispatch(addFocusPassRegistration()),
        onValueNameRegistration: value => dispatch(addValueNameRegistration(value)),
        onValuePassRegistration: value => dispatch(addValuePassRegistration(value)),
        onRegistration: () => dispatch(addRegistration()),
        onAuthorized: () => dispatch(addAuthorized()),
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (FormRegistration);
