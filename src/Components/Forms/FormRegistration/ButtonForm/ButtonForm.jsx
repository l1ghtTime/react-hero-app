import React from 'react';
import { useHistory } from 'react-router-dom';
import '../../../Forms/Forms.sass';

const ButtonForm = (props) => {

    const history = useHistory();
    
    const { isValueNameRegistration, isValuePassRegistration, onRegistration, onAuthorized, isFocusUsernameRegistration, onFocusUsernameRegistration, isFocusPassRegistration, onFocusPassRegistration } = props.store;
    

    function handleClick(e) {

        if(isValueNameRegistration.length > 0 && isValuePassRegistration.length > 0) {
            e.preventDefault();
            history.push("/profile");
            localStorage.setItem('Name', isValueNameRegistration);
            localStorage.setItem('Pass', isValuePassRegistration);

            onFocusUsernameRegistration();
            onFocusPassRegistration();

            onRegistration();
            onAuthorized();
        }
    }   

    return (
        <div className="area-btn">
            <input type="submit"
                className="area-btn__submit"
                style={isFocusUsernameRegistration === true && isFocusPassRegistration === true ? { border: '2px solid #dfaf3b', color: '#dfaf3b' } : null}
                onClick={handleClick}
            />
        </div>
    );
}

export default ButtonForm;