import React from 'react';
import '../../../Forms/Forms.sass';

const PassArea = (props) => {
    
    const { isFocusPassRegistration, onFocusPassRegistration, onValuePassRegistration, } = props.store;

    const classesFocusPass = ['area__label'];


    if(isFocusPassRegistration) {
        classesFocusPass.push('area-focus');
    }


    return (
        <div className="area">
            <label htmlFor="password" className={classesFocusPass.join(' ')} style={isFocusPassRegistration ? {color: '#dfaf3b'} : {color: '#b6b0b0'}} >
                Password:
            </label>

            <input type="password"
                id="password"
                style={isFocusPassRegistration ? {borderBottom: '2px solid #dfaf3b'} : null}
                className="area__input"
                autoComplete="on"
                required
                onFocus={event => event.target.value ? null : onFocusPassRegistration()}
                onBlur={event => event.target.value ? null : onFocusPassRegistration()}
                onChange={event => event.target.value.length >= 0 ? onValuePassRegistration(event.target.value) : null }
                
            />
        </div>
    );
}

export default PassArea;