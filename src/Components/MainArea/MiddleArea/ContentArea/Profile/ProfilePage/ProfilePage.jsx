import React from 'react';
import ProfileLeftBar from './ProfileLeftBar/ProfileLeftBar';
import ProfileRightBar from './ProfileRightBar/ProfileRightBar';


const ProfilePage = () => {
    return (
        <div className="profile__page">

            <div className="container">
                <div className="row">

                    <div className="col-xl-4 col-lg-5">
                        <ProfileLeftBar />
                    </div>

                    <div className="col-xl-8 col-lg-7">
                        <ProfileRightBar />
                    </div>

                </div>
            </div>

        </div>
    );
};

export default ProfilePage;