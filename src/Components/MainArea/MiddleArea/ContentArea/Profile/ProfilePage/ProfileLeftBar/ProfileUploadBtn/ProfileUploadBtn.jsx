import React from 'react';

const ProfileUploadBtn = (props) => {

    const {onProfileImgUrl, } = props.store; 

    function handleClick(event) {
        let url = URL.createObjectURL(event.target.files[0]);
        
        onProfileImgUrl(url);
    }

    return (
        <div className="profile__box">
            <label htmlFor="profile__upload" className="profile__custom">UPLOAD IMAGE</label>
            <input type="file" name="filename" id="profile__upload" className="profile__upload" onChange={handleClick}/>
        </div> 
    );
};

export default ProfileUploadBtn;