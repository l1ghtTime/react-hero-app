import React from 'react';

const ProfileImg = (props) => {

    const {isProfileImgUrl} = props.store; 

    return (
        <div className="profile__img" style={{background: `url( ${isProfileImgUrl} )`}}></div>
    );
};

export default ProfileImg;
