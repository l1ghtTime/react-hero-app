import React from 'react';
import ProfileImg from './ProfileImg/ProfileImg';
import ProfileUploadBtn from './ProfileUploadBtn/ProfileUploadBtn';
import { connect } from 'react-redux';
import { addProfileImageUrl } from '../../../../../../../redux/actions/actionProfileImageUrl';
import { addAuthorized } from '../../../../../../../redux/actions/actionsFormAuthorization';



const ProfileLeftBar = (props) => {
    return (
        <div className="profile__leftbar">
            <ProfileImg store={props} />
            <ProfileUploadBtn store={props} />
        </div>
    );
};


function mapStateToProps(state) {
    return {
        isProfileImgUrl: state.reducerProfileImageUrl.isProfileImgUrl,
        isAuthorized: state.reducerFormAuthorization.isAuthorized,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onProfileImgUrl: value => dispatch(addProfileImageUrl(value)),
        onAuthorized: () => dispatch(addAuthorized()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ProfileLeftBar);