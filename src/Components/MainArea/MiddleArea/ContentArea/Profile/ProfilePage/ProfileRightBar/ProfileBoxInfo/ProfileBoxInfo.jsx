import React from 'react';

const ProfileBoxInfo = (props) => {

    return (
        <div className="profile__info">

            <div className="profile__line">
                <span className="profile__item">
                    Username:
            </span>

                <span className="profile__item">
                    {localStorage.getItem('Name')}
                </span>
            </div>


            <div className="profile__line">
                <span className="profile__item">
                    Password:
            </span>

                <span className="profile__item">
                    {localStorage.getItem('Pass')}
                </span>
            </div>

        </div>
    );
};

export default ProfileBoxInfo;