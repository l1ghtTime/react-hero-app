import React from 'react';
import ProfileBoxInfo from './ProfileBoxInfo/ProfileBoxInfo';

const ProfileRightBar = () => {
    return (
        <div className="profile__rightbar">
            <ProfileBoxInfo />
        </div>
    );
};

export default ProfileRightBar;