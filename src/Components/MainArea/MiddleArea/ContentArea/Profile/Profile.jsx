import React from 'react';
import './Profile.sass';
import ProfilePage from './ProfilePage/ProfilePage';


const Profile = () => {
    return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="profile">
                        <ProfilePage />
                    </div>
                </div>
            </div>
    );
};

export default Profile;