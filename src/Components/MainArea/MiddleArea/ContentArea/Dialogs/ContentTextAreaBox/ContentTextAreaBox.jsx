import React from 'react';
import WarningMessage from '../../../../../Messages/WarningMessage/WarningMessage';

export const ContentTextAreaBox = (props, attr) => {

    const { isPostMessage, onPostMessage, onPost, isWarningMessage, onWarningMessage, isTypeMessage, onTypeMessage } = props.store;

    function handleClick(event) {

        let elem = event.target;

        attr = elem.getAttribute("data-message");

        onTypeMessage(attr);


        if (isPostMessage.length > 0) {
            onPost({
                message: isPostMessage
            });
        } else {

            onWarningMessage();

            setTimeout(() => {
                onWarningMessage();
            }, 2000);
        }
    }

    return (
        <div className="row">
            <div className="col-md-12 col-lg-10 mx-auto">
                <div className="coments">
                    <textarea className="coments__area" placeholder="Add coments" onChange={event => onPostMessage(event.target.value)} />
                    <input type="submit" className="coments__btn" value="Add coment" data-message="dialogs" onClick={handleClick} />

                    {isWarningMessage === true && isTypeMessage === 'dialogs'
                        ? <div className="area  coments__warning">
                            <WarningMessage text="Print value!" />
                        </div>
                        : null
                    }
                </div>
            </div>
        </div>

    );
};