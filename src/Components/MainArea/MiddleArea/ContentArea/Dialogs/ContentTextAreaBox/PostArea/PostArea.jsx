import React from 'react';
import Post from './Post/Post';

const PostArea = (props) => {

    const postsElements = props.store.isPost.map((post, index) => {

        return <Post key={index} id={index} store={props.store} message={post.message}/>
        
    });

    return (
        <div className="posts-area">
            {postsElements}
        </div>
    );
};

export default PostArea;