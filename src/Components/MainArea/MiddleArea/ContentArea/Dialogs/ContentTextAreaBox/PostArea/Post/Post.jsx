import React from 'react';
import imgProfile from '../../../../../../../../icons/person.png';
import imgClose from '../../../../../../../../icons/close.png';

const Post = (props) => {

    const { isProfileImgUrl, onRmPosts } = props.store;

    function getWeekDay(date) {
        let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      
        return days[date.getDay()];
    }

    function getMounse(date) {
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', ];
      
        return months[date.getMonth()];
    }

    let date = new Date();

    let minutes = date.getMinutes(),
        hour = date.getHours(),
        week = getWeekDay(date),
        day = date.getDate(),
        month = getMounse(date),
        year = date.getFullYear();

    function handleClickClose() {
        onRmPosts(props.id);
    }

    return (
        <div className="posts-area__item">
            <div className="posts-area__content">
                {isProfileImgUrl.length > 0
                    ? <div className="posts-area__image navigation__profile" style={{ background: `url( ${isProfileImgUrl} )` }}></div>
                    : <img className="posts-area__image navigation__profile" src={imgProfile} alt="lock-icon" />
                }
                <div>
                    <span className="posts-area__text">
                        {props.message}
                    </span>
                </div>

                <img src={imgClose} className="posts-area__close" alt="close" onClick={handleClickClose} />
            </div>


            <div className="posts-area__timer">
                {`Published: ${hour} : ${minutes} ${week} ${day} ${month} ${year}`}
            </div>
        </div>
    );
};

export default Post;