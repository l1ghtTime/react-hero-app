import React from 'react';
import './Dialogs.sass';
import { ContentTextAreaBox } from './ContentTextAreaBox/ContentTextAreaBox';
import PostArea from './ContentTextAreaBox/PostArea/PostArea';
import { connect } from 'react-redux';
import { addProfileImageUrl } from '../../../../../redux/actions/actionProfileImageUrl';
import { addPostMessage, addPost, addRemovePost } from '../../../../../redux/actions/actionPosts';
import { addWarningMessage, addTypeMessage } from '../../../../../redux/actions/actionWarningMessage';


const Dialogs = (props) => {

    return (
        <div className="dialog">
            <React.Fragment>
                <ContentTextAreaBox store={props} />
                <PostArea store={props} />
            </React.Fragment>
        </div>
    );
}


function mapStateToProps(state) {

    return {
        isProfileImgUrl: state.reducerProfileImageUrl.isProfileImgUrl,
        isPostMessage: state.reducerPosts.postMessage,
        isPost: state.reducerPosts.posts,
        isWarningMessage: state.reducerWarningMessage.isWarningMessage,
        isTypeMessage: state.reducerWarningMessage.isTypeMessage,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onProfileImgUrl: value => dispatch(addProfileImageUrl(value)),
        onPostMessage: value => dispatch(addPostMessage(value)),
        onPost: value => dispatch(addPost(value)),
        onWarningMessage: () => dispatch(addWarningMessage()),
        onTypeMessage: value => dispatch(addTypeMessage(value)),
        onRmPosts: value => dispatch(addRemovePost(value)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (Dialogs);