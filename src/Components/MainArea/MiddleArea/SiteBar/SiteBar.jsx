import React from 'react';
import './SiteBar.sass'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addRegistration } from '../../../../redux/actions/actionsFormRegistration';
import { addAuthorized } from '../../../../redux/actions/actionsFormAuthorization';
import WarningMessage from '../../../Messages/WarningMessage/WarningMessage';
import { addWarningMessage, addTypeMessage } from '../../../../redux/actions/actionWarningMessage';

const SiteBar = (props, attr) => {

    function handleClick(event) {

        let elem = event.target;

        attr = elem.getAttribute("data-message");

        props.onTypeMessage(attr);


        if (props.isRegistration === false || (props.isAuthorized === false && props.isRegistration === true)) {
            
            props.onWarningMessage();

            setTimeout(() => {
                props.onWarningMessage();
            }, 2000);
        }

       
    }

    return (
        <nav className="sitebar">
            <ul className="sitebar-list">
                <li className="sitebar-list__item">
                    <Link to="/" className="sitebar-list__link">
                        Home
                    </Link>
                </li>

                <li className="sitebar-list__item">
                    {props.isAuthorized && props.isRegistration
                        ? <Link to="/dialogs" className="sitebar-list__link">Dialog</Link>
                        : <span className="sitebar-list__link" data-message="sidebar" onClick={ handleClick }>Dialog</span>
                    }
                </li>

                <li className="sitebar-list__item">
                    {
                        props.isAuthorized && props.isRegistration
                        ? <Link to="/profile" className="sitebar-list__link">Profile</Link>
                        : <span className="sitebar-list__link" data-message="sidebar" onClick={ handleClick }>Profile</span>
                    }

                    {props.isWarningMessage && (props.isTypeMessage === "sidebar") ? <WarningMessage text="You should authorization!"/> : null}
                </li>
            </ul>
        </nav>
    );
}


function mapStateToProps(state) {

    return {
        isAuthorized: state.reducerFormAuthorization.isAuthorized,
        isRegistration: state.reducerFormRegistration.isRegistration,
        isWarningMessage: state.reducerWarningMessage.isWarningMessage,
        isTypeMessage: state.reducerWarningMessage.isTypeMessage,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAuthorized: () => dispatch(addAuthorized()),
        onRegistration: () => dispatch(addRegistration()),
        onWarningMessage: () => dispatch(addWarningMessage()),
        onTypeMessage: value => dispatch(addTypeMessage(value)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SiteBar);