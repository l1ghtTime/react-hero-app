import React from 'react';
import './Header.sass';
import { Logo } from './Logo/Logo';
import AuthorizationLink from './AuthorizationLink/AuthorizationLink';

export const Header = () => {

    return (
        <header className="header">
            <div className="container">
                <div className="row">
                    <div className='col-2 col-sm-2 col-md-2 col-lg-2'>
                        <Logo />
                    </div>

                    <div className='col-3 offset-5 col-sm-3 offset-sm-6 col-md-3 offset-md-6 col-lg-2 offset-lg-7' >
                        <AuthorizationLink />
                    </div>
                </div>
            </div>
        </header>
    );
};