import React from 'react';
import imgLock from '../../../../icons/lock.png';
import imgExit from '../../../../icons/exit.png';
import imgRegistration from '../../../../icons/registration.png';
import imgProfile from '../../../../icons/person.png';
import './AuthorizationLink.sass';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addAuthorized, addFocusName, addFocusPass, addValueName, addValuePass } from '../../../../redux/actions/actionsFormAuthorization';
import WarningMessage from '../../../Messages/WarningMessage/WarningMessage';
import SuccessMessage from '../../../Messages/SuccessMessage/SuccessMessage';
import { addWarningMessage, addTypeMessage } from '../../../../redux/actions/actionWarningMessage';
import { addSuccessMessage, addTypeSuccessMessage } from '../../../../redux/actions/actionSuccessMessage';
import { addProfileImageUrl, } from '../../../../redux/actions/actionProfileImageUrl';


const AuthorizationLink = (props, attr) => {

    function handleClick(event) {

        let elem = event.target;

        attr = elem.getAttribute("data-message");

        props.onTypeMessage(attr);

        if (props.isRegistration === false || (props.isAuthorized === false && props.isRegistration === true)) {
            
            props.onWarningMessage();

            setTimeout(() => {
                props.onWarningMessage();
            }, 2000);
        }
    }

    function handleClickSuccess(event) {

        let elem = event.target;

        attr = elem.getAttribute("data-message");
        
        props.onTypeSuccessMessage(attr);
        
        if(props.isRegistration === true) {
            props.onSuccessMessage();

            setTimeout(() => {
                props.onSuccessMessage();
            }, 2000);
        }
    }

    function handleClickExit() {
        props.onAuthorized();

        props.onValueName('');
        props.onValuePass('');

    }


    return (

        <nav className="navigation">
            <div className="navigation__box">
                <img className="navigation__img" src={props.isAuthorized === false ? imgLock : imgExit} alt="lock-icon" />

                <div className="navigation__title">
                    {props.isAuthorized === false 
                        ? <Link to="authorization" className="navigation__link">Authorization</Link>
                        : <Link to='/' className="navigation__link" onClick={handleClickExit}>Exit</Link>
                    }   
                </div>
            </div>



            <div className="navigation__box">
                <img className="navigation__img" src={imgRegistration} alt="lock-icon" />



                <div className="navigation__title">

                    {props.isRegistration === false
                        ? <Link to="/registration" className="navigation__link">Registration</Link>
                        : <span className="navigation__link" data-message="headerNavRegistration" onClick={handleClickSuccess}>Registration</span>
                    }
                </div>
            </div>

            <div className="navigation__box">
                {props.isAuthorized === true && props.isProfileImgUrl.length > 0
                    ? <div className="navigation__profile" style={{background: `url( ${props.isProfileImgUrl} )`}} />
                    : <img className="navigation__img" src={imgProfile} alt="lock-icon" />
                }

                <div className="navigation__title">
                    {props.isRegistration && props.isAuthorized         
                    ? <Link to="/profile" className="navigation__link">Profile</Link>

                    : <span className="navigation__link" data-message="headerNav" onClick={handleClick}>Profile</span>

                    }
                </div>


                <div className="navigation__warning">
                    {props.isWarningMessage && (props.isTypeMessage === "headerNav") ? <WarningMessage text="You should authorization!"/> : null}
                </div>


                <div className="navigation__success">
                    {props.isSuccessMessage && (props.isTypeSuccessMessage === "headerNavRegistration") ? <SuccessMessage text="You registrated!"/> : null}
                </div>

            </div>
        </nav>
    );
};


function mapStateToProps(state) {
    return {
        isAuthorized: state.reducerFormAuthorization.isAuthorized,
        isRegistration: state.reducerFormRegistration.isRegistration,
        isWarningMessage: state.reducerWarningMessage.isWarningMessage,
        isTypeMessage: state.reducerWarningMessage.isTypeMessage,
        isSuccessMessage: state.reducerSuccessMessage.isSuccessMessage,
        isTypeSuccessMessage: state.reducerSuccessMessage.isTypeSuccessMessage,
        isFocusUsername: state.reducerFormAuthorization.isFocusUsername,
        isFocusPass: state.reducerFormAuthorization.isFocusPass,
        isValueName: state.reducerFormAuthorization.isValueName,
        isValuePass: state.reducerFormAuthorization.isValuePass,
        isProfileImgUrl: state.reducerProfileImageUrl.isProfileImgUrl,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAuthorized: () => dispatch(addAuthorized()),
        onWarningMessage: () => dispatch(addWarningMessage()),
        onTypeMessage: value => dispatch(addTypeMessage(value)),
        onSuccessMessage: () => dispatch(addSuccessMessage()),
        onTypeSuccessMessage: value => dispatch(addTypeSuccessMessage(value)),
        onFocusName: () => dispatch(addFocusName()),
        onFocusPass: () => dispatch(addFocusPass()),
        onValueName: value => dispatch(addValueName(value)),
        onValuePass: value => dispatch(addValuePass(value)),
        onProfileImgUrl: value => dispatch(addProfileImageUrl(value)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationLink);

