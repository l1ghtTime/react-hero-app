import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.css';
import './index.sass';
import { App } from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './redux/rootReducer';
import { BrowserRouter as Router } from 'react-router-dom';
import {logger} from "redux-logger";

export const store = createStore(rootReducer, applyMiddleware(logger,reduxThunk));

const app = (
  <Router>
    <Provider store={store}>
      <App />
    </Provider>
  </Router>
);

ReactDOM.render(
  app,
  document.getElementById('root')
);

serviceWorker.unregister();
