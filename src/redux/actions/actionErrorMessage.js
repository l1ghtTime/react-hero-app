import { ADD_ERROR_MESSAGE, } from "./actionTypes"


export function addErrorMessage() {
    return {
        type: ADD_ERROR_MESSAGE,
    }
}

