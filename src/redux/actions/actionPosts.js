import { ADD_POSTS_MESSAGE, ADD_POST, REMOVE_POST } from "./actionTypes"


export function addPostMessage(value) {
    return {
        type: ADD_POSTS_MESSAGE,
        payload: value,
    }
}

export function addPost(value) {

    return {
        type: ADD_POST,
        payload: value,
    }
}

export function addRemovePost(value) {

    console.log('VALUE', value);
    return {
        type: REMOVE_POST,
        payload: value,
    }
}