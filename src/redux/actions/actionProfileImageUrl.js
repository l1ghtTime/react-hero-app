import { ADD_PROFILE_IMAGE_URL, } from "./actionTypes"


export function addProfileImageUrl(value) {
    return {
        type: ADD_PROFILE_IMAGE_URL,
        payload: value,
    }
}