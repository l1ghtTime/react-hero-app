import { ADD_SUCCESS_MESSAGE, ADD_TYPE_SUCCESS_MESSAGE } from "./actionTypes"


export function addSuccessMessage() {
    return {
        type: ADD_SUCCESS_MESSAGE,
    }
}


export function addTypeSuccessMessage(value) {
    return {
        type: ADD_TYPE_SUCCESS_MESSAGE,
        payload: value,
    }
}
