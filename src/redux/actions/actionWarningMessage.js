import { ADD_WARNING_MESSAGE, ADD_TYPE_MESSAGE } from "./actionTypes"


export function addWarningMessage() {
    return {
        type: ADD_WARNING_MESSAGE,
    }
}


export function addTypeMessage(value) {
    return {
        type: ADD_TYPE_MESSAGE,
        payload: value,
    }
}
