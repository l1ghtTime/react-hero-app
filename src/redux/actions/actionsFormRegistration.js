import {ADD_FOCUS_NAME_REGISTRATION, ADD_FOCUS_PASS_REGISTRATION, ADD_VALUE_NAME_REGISTRATION, ADD_VALUE_PASS_REGISTRATION, ADD_REGISTRATION,  } from './actionTypes';

export function addFocusNameRegistration() {
    return {
        type: ADD_FOCUS_NAME_REGISTRATION,
    }
}

export function addFocusPassRegistration() {
    return {
        type: ADD_FOCUS_PASS_REGISTRATION,
    }
}

export function addValueNameRegistration(value){
    return {
        type: ADD_VALUE_NAME_REGISTRATION,
        payload: value,
    }
}

export function addValuePassRegistration(value){
    return {
        type: ADD_VALUE_PASS_REGISTRATION,
        payload: value,
    }
}

export function addRegistration(){
    return {
        type: ADD_REGISTRATION,
    }
}

