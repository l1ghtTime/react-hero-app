import { ADD_POSTS_MESSAGE, ADD_POST, REMOVE_POST, } from "../actions/actionTypes";


const initialState = {
    postMessage: '',
    posts: [],
}

const reducerPosts = (state = initialState, action) => {

    switch (action.type) {

        case ADD_POSTS_MESSAGE:
            return {
                ...state,
                postMessage: action.payload
            }

        case ADD_POST:
            let newPosts = [...state.posts];
            newPosts.unshift(action.payload);
            return {
                ...state,
                posts: newPosts,
            }

        case REMOVE_POST:
            return {
                ...state,
                posts: state.posts.filter((item, index) => index !== action.payload),
            }

        default:
            return state;
    }

};

export default reducerPosts;
