import { ADD_PROFILE_IMAGE_URL, } from "../actions/actionTypes";


const initialState = {
    isProfileImgUrl: '',
}

const reducerProfileImageUrl = (state = initialState, action) => {

    switch (action.type) {

        case ADD_PROFILE_IMAGE_URL:
            return {
                ...state,
                isProfileImgUrl: action.payload,
            }

        default:
            return state;
    }

};

export default reducerProfileImageUrl;
