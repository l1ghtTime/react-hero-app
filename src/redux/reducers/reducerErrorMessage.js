import { ADD_ERROR_MESSAGE, } from "../actions/actionTypes";


const initialState = {
    isErrorMessage: false,
}

const reducerErrorMessage = (state = initialState, action) => {

    switch (action.type) {

        case ADD_ERROR_MESSAGE:
            return {
                ...state,
                isErrorMessage: !state.isErrorMessage,
            }

        default:
            return state;
    }

};

export default reducerErrorMessage;
