import { ADD_FOCUS_NAME_REGISTRATION, ADD_FOCUS_PASS_REGISTRATION, ADD_VALUE_NAME_REGISTRATION, ADD_VALUE_PASS_REGISTRATION, ADD_REGISTRATION, } from "../actions/actionTypes";


const initialState = {
    isFocusUsernameRegistration: false,
    isFocusPassRegistration: false,
    isValueNameRegistration: '',
    isValuePassRegistration: '',
    isRegistration: false,
}

const reducerFormRegistration = (state = initialState, action) => {

    switch (action.type) {

        case ADD_FOCUS_NAME_REGISTRATION:
            return {
                ...state,
                isFocusUsernameRegistration: !state.isFocusUsernameRegistration,
            };

        case ADD_FOCUS_PASS_REGISTRATION:
            return {
                ...state,
                isFocusPassRegistration: !state.isFocusPassRegistration,
            }

        case ADD_VALUE_NAME_REGISTRATION:
            return {
                ...state,
                isValueNameRegistration: action.payload,
            }

        case ADD_VALUE_PASS_REGISTRATION:
            return {
                ...state,
                isValuePassRegistration: action.payload,
            }

        case ADD_REGISTRATION:
            return {
                ...state,
                isRegistration: !state.isRegistration,
            }

        default:
            return state;
    }

};

export default reducerFormRegistration;