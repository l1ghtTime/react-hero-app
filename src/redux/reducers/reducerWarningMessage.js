import { ADD_WARNING_MESSAGE, ADD_TYPE_MESSAGE, } from "../actions/actionTypes";


const initialState = {
    isWarningMessage: false,
    isTypeMessage: '',
}

const reducerWarningMessage = (state = initialState, action) => {

    switch (action.type) {

        case ADD_WARNING_MESSAGE:
            return {
                ...state,
                isWarningMessage: !state.isWarningMessage,
            }


        case ADD_TYPE_MESSAGE:
            return {
                ...state,
                isTypeMessage: action.payload,
            }


        default:
            return state;
    }

};

export default reducerWarningMessage;
