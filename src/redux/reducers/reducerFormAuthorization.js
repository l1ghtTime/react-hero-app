import { ADD_FOCUS_NAME, ADD_FOCUS_PASS, ADD_VALUE_NAME, ADD_VALUE_PASS, ADD_AUTHORIZED } from "../actions/actionTypes";


const initialState = {
    isFocusUsername: false,
    isFocusPass: false,
    isValueName: '',
    isValuePass: '',
    isPassInerHtml: '',
    isAuthorized: false,
}

const reducerFormAuthorization = (state = initialState, action) => {

    switch (action.type) {

        case ADD_FOCUS_NAME:
            return {
                ...state,
                isFocusUsername: !state.isFocusUsername,
            };

        case ADD_FOCUS_PASS:
            return {
                ...state,
                isFocusPass: !state.isFocusPass,
            }

        case ADD_VALUE_NAME:
            return {
                ...state,
                isValueName: action.payload,
            }

        case ADD_VALUE_PASS:
            return {
                ...state,
                isValuePass: action.payload
            }

        case ADD_AUTHORIZED:
            return {
                ...state,
                isAuthorized: !state.isAuthorized
            }

        default:
            return state;
    }

};

export default reducerFormAuthorization;