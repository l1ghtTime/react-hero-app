import { ADD_SUCCESS_MESSAGE, ADD_TYPE_SUCCESS_MESSAGE, } from "../actions/actionTypes";


const initialState = {
    isSuccessMessage: false,
    isTypeSuccessMessage: '',
}

const reducerSuccessMessage = (state = initialState, action) => {

    switch (action.type) {

        case ADD_SUCCESS_MESSAGE:
            return {
                ...state,
                isSuccessMessage: !state.isSuccessMessage,
            }


        case ADD_TYPE_SUCCESS_MESSAGE:
            return {
                ...state,
                isTypeSuccessMessage: action.payload,
            }


        default:
            return state;
    }

};

export default reducerSuccessMessage;
