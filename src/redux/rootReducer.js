import { combineReducers } from 'redux';
import reducerFormAuthorization from '../redux/reducers/reducerFormAuthorization';
import reducerFormRegistration from '../redux/reducers/reducerFormRegistration';
import reducerWarningMessage from '../redux/reducers/reducerWarningMessage';
import reducerErrorMessage from '../redux/reducers/reducerErrorMessage';
import reducerSuccessMessage from '../redux/reducers/reducerSuccessMessage';
import reducerProfileImageUrl from '../redux/reducers/reducerProfileImageUrl';
import reducerPosts from '../redux/reducers/reducerPosts';

export default combineReducers({
    reducerFormAuthorization, reducerFormRegistration, reducerWarningMessage, reducerErrorMessage, reducerSuccessMessage, reducerProfileImageUrl, reducerPosts,
});