import React from 'react';
import './App.sass';
import { MainArea } from './Components/MainArea/MainArea';
import { Switch, Route } from 'react-router-dom';
import FormАuthorization from './Components/Forms/FormAuthorization/FormАuthorization';
import FormRegistration from './Components/Forms/FormRegistration/FormRegistration';

export function App() {

  return (
    <React.Fragment>

      <Switch>
        <Route path='/authorization' component={FormАuthorization} />
        <Route path='/registration' component={FormRegistration} />
        <Route path='/' component={MainArea} />
      </Switch>
      
    </React.Fragment>
  );
}